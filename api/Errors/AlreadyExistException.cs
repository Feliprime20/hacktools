﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrotoolsHacktoolsApi.Errors
{
    public class AlreadyExistException: Exception
    {
        public AlreadyExistException(string? message) : base(message)
        {

        }
    }
}
