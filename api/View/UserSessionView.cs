﻿using AgrotoolsHacktoolsApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrotoolsHacktoolsApi.View
{
    public class UserSessionView
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public DateTime Date { get; set; }

        public string Token { get; set; }

        public UserSessionView(UserSession userSession)
        {
            Id = userSession.User.Id;
            Name = userSession.User.Name;
            Email = userSession.User.Email;
            Date = userSession.User.Date;
            Token = userSession.Token;
        }
    }
}
