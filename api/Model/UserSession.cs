﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrotoolsHacktoolsApi.Model
{
    public class UserSession
    {
        public User User { get; set; }
        public string Token { get; set; }
    }
}
