﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AgrotoolsHacktoolsApi.Model
{
    public class Credentials
    {
        [Required(ErrorMessage = "Informe o e-mail", AllowEmptyStrings = false)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Informe a senha", AllowEmptyStrings = false)]
        public string Password { get; set; }
    }
}
