﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AgrotoolsHacktoolsApi.Model
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Informe o nome", AllowEmptyStrings = false)]
        [MinLength(length: 3, ErrorMessage = "O nome deve ter pelo menos 3 caracteres")]
        [MaxLength(length: 100, ErrorMessage = "O nome deve ter no máximo 100 caracteres")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Informe o email", AllowEmptyStrings = false)]
        [MinLength(length: 6, ErrorMessage = "O email deve ter pelo menos 6 caracteres")]
        [MaxLength(length: 254, ErrorMessage = "O email deve ter no máximo 254 caracteres")]
        [EmailAddress(ErrorMessage = "Informe um e-mail válido")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Informe a senha", AllowEmptyStrings = false)]
        [MinLength(length: 6, ErrorMessage = "A senha deve ter pelo menos 6 caracteres")]
        [MaxLength(length: 20, ErrorMessage = "A senha deve ter no máximo 20 caracteres")]
        public string Password { get; set; }

        public DateTime Date { get; set; }

        public User()
        {
            this.Date = DateTime.Now;
        }
    }
}
