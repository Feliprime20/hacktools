﻿using AgrotoolsHacktoolsApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrotoolsHacktoolsApi.Repository
{
    public interface IUserRepository: IRepository<User>
    {
        public User FindByEmail(string email);
    }
}
