﻿using AgrotoolsHacktoolsApi.Model;
using AgrotoolsHacktoolsApi.Repository;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrotoolsHacktoolsApi.Service
{
    public class UserRepositoryMySql : IUserRepository
    {
        private MySQLDb Db;

        public UserRepositoryMySql(MySQLDb db)
        {
            this.Db = db;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public User Find(int id)
        {
            throw new NotImplementedException();
        }

        public List<User> FindAll()
        {
            throw new NotImplementedException();
        }

        public User FindByEmail(string email)
        {
            User user = null;
            var connection = this.Db.Connection;
            connection.Open();
            using (var cmd = new MySqlCommand())
            {
                cmd.Connection = connection;
                cmd.CommandText = "SELECT * FROM tb_users WHERE email = @email";
                cmd.Parameters.AddWithValue("email", email);
                var dataReader = cmd.ExecuteReader();
                
                
                if(dataReader.Read())
                {
                    user = new User()
                    {
                        Name = dataReader.GetString("nome"),
                        Email = dataReader.GetString("email"),
                        Password = dataReader.GetString("senha"),
                        Date = dataReader.GetDateTime("data"),
                        Id = dataReader.GetInt32("id"),
                    };

                }
                dataReader.Close();
            }
            connection.Close();

            return user;
        }

        public User Insert(User model)
        {
            var connection = this.Db.Connection;
            connection.Open();

            using (var cmd = new MySqlCommand())
            {
                cmd.Connection = connection;
                
                cmd.CommandText = "INSERT INTO tb_users (nome, email, senha, data) VALUES (@nome, @email, @senha, @data);";
                cmd.Parameters.AddWithValue("nome", model.Name);
                cmd.Parameters.AddWithValue("email", model.Email);
                cmd.Parameters.AddWithValue("senha", model.Password);
                cmd.Parameters.AddWithValue("data", model.Date);

                cmd.ExecuteNonQuery();
                model.Id = (int) cmd.LastInsertedId;
            }

            connection.Close();

            return model;
        }

        public void Update(User model)
        {
            throw new NotImplementedException();
        }
    }
}
