﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrotoolsHacktoolsApi.Repository
{
    public interface IRepository<T>
    {
        /// <exception cref="Exception">Exceção é lançada caso ocorra algum erro durante a inserção</exception>
        public T Insert(T model);

        /// <exception cref="Exception">Exceção é lançada caso ocorra algum erro durante a consulta</exception>
        public T Find(int id);

        /// <exception cref="Exception">Exceção é lançada caso ocorra algum erro durante a consulta</exception>
        public List<T> FindAll();

        /// <exception cref="Exception">Exceção é lançada caso ocorra algum erro durante a exclusão</exception>
        public void Delete(int id);

        /// <exception cref="Exception">Exceção é lançada caso ocorra algum erro durante a alteração</exception>
        public void Update(T model);
    }
}
