﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrotoolsHacktoolsApi.Service
{
    public class MySQLDb : IDisposable
    {
        public MySqlConnection Connection { get; }

        public MySQLDb(MySqlConnection connection)
        {
            Connection = connection;
        }

        public void Dispose() => Connection.Dispose();
    }
}
