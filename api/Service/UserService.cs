﻿using AgrotoolsHacktoolsApi.Errors;
using AgrotoolsHacktoolsApi.Model;
using AgrotoolsHacktoolsApi.Repository;
using AgrotoolsHacktoolsApi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrotoolsHacktoolsApi.Service
{
    public class UserService
    {
        private readonly IUserRepository _userRepository;
        private readonly PasswordHashService _passwordHashService;
        private readonly TokenService _tokenService;

        public UserService(IUserRepository userRepository, PasswordHashService passwordHashService, TokenService tokenService)
        {
            _userRepository = userRepository;
            _passwordHashService = passwordHashService;
            _tokenService = tokenService;
        }

        public UserSession Register(User user)
        {
            user.Password = _passwordHashService.GetHash(user.Password);

            var existent = this._userRepository.FindByEmail(user.Email);

            if(existent != null)
            {
                throw new AlreadyExistException("Já Existe um usuário cadastrado com esse e-mail.");
            }

            var result = this._userRepository.Insert(user);
            return new UserSession() { User = result, Token = _tokenService.GenerateToken(result) };
        }

        public UserSession Login(Credentials credentials)
        {
            var existent = this._userRepository.FindByEmail(credentials.Email);
            

            if(existent == null || !_passwordHashService.Verify(credentials.Password, existent.Password))
            {
                throw new UnauthorizedAccessException();
            }

            return new UserSession() { User = existent, Token = _tokenService.GenerateToken(existent)};
        }
    }
}
