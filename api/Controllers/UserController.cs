﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgrotoolsHacktoolsApi.Errors;
using AgrotoolsHacktoolsApi.Model;
using AgrotoolsHacktoolsApi.Repository;
using AgrotoolsHacktoolsApi.Service;
using AgrotoolsHacktoolsApi.View;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AgrotoolsHacktoolsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult<UserSessionView> Create(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var result = _userService.Register(user);
                return Created("", new UserSessionView(result));
            }
            catch(Exception e)
            {
                if(e is AlreadyExistException)
                {
                    return Conflict(new { error = e.Message});
                }

                return Problem(e.Message);
            }
        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public ActionResult<UserSessionView> Login(Credentials credentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var result = _userService.Login(credentials);
                return Created("", new UserSessionView(result));
            }
            catch (Exception e)
            {
                if (e is UnauthorizedAccessException)
                {
                    return Unauthorized();
                }

                return Problem(e.Message);
            }
        }
    }
}
