# Agrotools Hacktools

Desafio Agrotools
APP React Native de questionários + API .NET Core.

# Tabela de conteúdos

- [Agrotools Hacktools](#agrotools-hacktools)
- [Tabela de conteúdos](#tabela-de-conteúdos)
  - [💻 Sobre o projeto](#-sobre-o-projeto)
  - [⚙️ Funcionalidades](#️-funcionalidades)
  - [- Excluir questionário (banco de dados local)](#--excluir-questionário-banco-de-dados-local)
  - [🚀 Como executar o projeto](#-como-executar-o-projeto)
    - [Pré-requisitos](#pré-requisitos)
      - [💾 Criando Banco de Dados (MySQL)](#-criando-banco-de-dados-mysql)
      - [🎲 Rodando a API (servidor)](#-rodando-a-api-servidor)
      - [📱 Rodando o APP](#-rodando-o-app)
  - [🛠 Tecnologias](#-tecnologias)
      - [**Server** (.NET Core 3.1)](#server-net-core-31)
      - [**Mobile** (React Native)](#mobile-react-native)
  - [🧑‍💻 Autor](#-autor)
  - [Felipe Andrade.](#felipe-andrade)
  - [📝 Licença](#-licença)

## 💻 Sobre o projeto

---

## ⚙️ Funcionalidades

- Criar questionário (banco de dados local)
- Responder um questionário (banco de dados local)
- Login
- Cadastro
- Editar Respostas do questionário (banco de dados local)
- Excluir questionário (banco de dados local)
---

## 🚀 Como executar o projeto

Este projeto é divido em duas partes:

1. API (pasta server)
2. Mobile (pasta mobile)

💡O app Mobile precisa que a API esteja rodando para funcionar.

### Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Node.js](https://nodejs.org/en/) e [.NET Core](https://dotnet.microsoft.com/download).

#### 💾 Criando Banco de Dados (MySQL)

```bash

# Clone este repositório
$ git clone git@bitbucket.org:Feliprime20/hacktools.git

# Importando banco de dados
$ mysql -u username -p < db_agrotools_hacktool.sql

```

#### 🎲 Rodando a API (servidor)

Configure a conexão com o banco de dados alterando o arquivo `appsettings.json` dentro da pasta **api**.

```json
...
"ConnectionStrings": {
    "Default": "server=[HOST_BD];user=[USER_BD];password=[SENHA_BD];database=db_agrotools_hacktool"
  },
...
```

```bash

# Clone este repositório
$ git clone git@bitbucket.org:Feliprime20/hacktools.git

# Vá para a pasta api
$ cd api

# Instale as dependências
$ dotnet restore

# Execute a aplicação
$ dotnet run

# O servidor inciará na porta:51734 - acesse http://localhost:51734

# Para Rodar os teste unitários
$ cd HacktoolsApiTest

# Instale as dependências
$ dotnet core restore

$ dotnet test
```

Configure a conexão com o banco de dados alterando o arquivo appsettings.json dentro da pasta **api**.

#### 📱 Rodando o APP


```bash

# Clone este repositório
$ git clone git@bitbucket.org:Feliprime20/hacktools.git

# Vá para a pasta app
$ cd app
```

Altere a URL da API no arquivo `src/config/index.js`.

```js

export default {
  baseUrl: 'http://[HOST]:51734/api/',
};
```

```bash

# Instale as dependências
$ yarn install

# Execute a aplicação em um device Android
$ npx react-native run-android 

```

## 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

#### **Server** (.NET Core 3.1)
- **[bcrypt.net - next](https://github.com/BcryptNet/bcrypt.net)**
- **[MySql.Data](https://www.nuget.org/packages/MySql.Data/)**
- **[Microsoft.AspNetCore.Authentication](https://www.nuget.org/packages/Microsoft.AspNetCore.Authentication/2.2.0?_src=template)**
- **[Microsoft.AspNetCore.Authentication.JwtBearer](https://www.nuget.org/packages/Microsoft.AspNetCore.Authentication.JwtBearer/3.1.12?_src=template)**
- **[Nunit](https://nunit.org/)**

#### **Mobile** (React Native)

- **[Axios](https://www.npmjs.com/package/axios)**
- **[React Navigation](https://reactnavigation.org/)**
- **[Vector Icons](https://github.com/oblador/react-native-vector-icons)**
- **[Realm](https://github.com/realm/realm-js)**
- **[uuid](https://github.com/uuidjs/uuid)**
- **[Yarn](https://yarnpkg.com/)**
- **[React-Native Get Location](https://github.com/douglasjunior/react-native-get-location)**

## 🧑‍💻 Autor

Felipe Andrade.
---

## 📝 Licença

Este projeto esta sobe a licença [MIT](./LICENSE.txt).

---