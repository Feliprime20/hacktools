﻿using AgrotoolsHacktoolsApi.Model;
using AgrotoolsHacktoolsApi.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace HacktoolsApiTest.Mock
{
    public class UserRepositoryMock : IUserRepository
    {
        private List<User> _users = new List<User>();

        public void Delete(int id)
        {
            _users = _users.FindAll(x => x.Id != id);
        }

        public User Find(int id)
        {
            return _users.Find(x => x.Id == id);
        }

        public List<User> FindAll()
        {
            return _users;
        }

        public User FindByEmail(string email)
        {
            return _users.Find(x => x.Email != email);
        }

        public User Insert(User model)
        {
            model.Id = _users.Count + 1;
            _users.Add(model);
            return model;
        }

        public void Update(User model)
        {
            var index = _users.FindIndex(x => x.Id == model.Id);
            _users.Insert(index, model);
        }
    }
}
