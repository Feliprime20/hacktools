﻿using AgrotoolsHacktoolsApi.Model;
using AgrotoolsHacktoolsApi.Service;
using HacktoolsApiTest.Mock;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace HacktoolsApiTest.Service
{
    class LoginTest
    {
        private UserService _userService;

        [SetUp]
        public void Setup()
        {
            var tokenService = new TokenService("lgGR3PNNJha2BVIoUe4Ek96sn6hDDKbX");
            var hashService = new PasswordHashService();
            var userRepository = new UserRepositoryMock();
            
            userRepository.Insert(new User() { 
                Name = "Test", 
                Email = "test@gmail.com", 
                Date = DateTime.Now, 
                Id = 1, 
                Password = hashService.GetHash("123456")
            });

            _userService = new UserService(userRepository, hashService, tokenService);
        }

        [Test]
        public void UserThatNotExistsCantLogin()
        {
            Assert.Throws<UnauthorizedAccessException>(() => _userService.Login(new Credentials() { Email = "naoexiste@gmail.com", Password = "123" }));
        }

        [Test]
        public void UserWithWrongCredentialsCantLogin()
        {
            Assert.Throws<UnauthorizedAccessException>(() => _userService.Login(new Credentials() { Email = "test@gmail.com", Password = "123" }));
        }
    }
}
