import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

const FAB = ({style, buttonStyle, onPress, name, size, color}) => {
  return (
    <TouchableOpacity onPress={onPress} style={style}>
      <View style={[styles.button, buttonStyle]}>
        <Icon name={name} size={size} color={color} />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    borderRadius: 32,
    width: 56,
    height: 56,
    padding: 4,
    backgroundColor: '#620076',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default FAB;
