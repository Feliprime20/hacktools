import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import IconButton from './IconButton';

const SurveyCard = ({
  survey,
  deleteAction,
  configureAction,
  answerAction,
  hasAnswer,
  editAnswersAction,
}) => {
  return (
    <View style={styles.card}>
      <View style={styles.cardContent}>
        <Icon name="file-text" size={24} color="#ccc" />
        <View style={styles.content}>
          <Text>{survey.title}</Text>
          <Text>Questões {survey.questions.length}</Text>
        </View>
        <IconButton
          name="trash"
          size={18}
          color="#f44336"
          onPress={() => deleteAction(survey)}
        />
      </View>
      {!hasAnswer && (
        <View style={styles.actions}>
          <IconButton
            name="settings"
            size={18}
            text="Configurar"
            color="#620076"
            style={styles.actionButton}
            onPress={() => configureAction(survey)}
          />

          {survey.questions.length > 0 && (
            <IconButton
              name="edit-2"
              size={18}
              text="Responder"
              color="#620076"
              style={styles.actionButton}
              onPress={() => answerAction(survey)}
            />
          )}
        </View>
      )}
      {hasAnswer && (
        <IconButton
          name="check"
          size={18}
          text="Respondido"
          color="#4caf50"
          style={styles.answered}
          onPress={() => editAnswersAction(survey)}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 16,
    borderRadius: 12,
    marginVertical: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  cardContent: {
    flexDirection: 'row',
  },
  actions: {
    flexDirection: 'row',
    marginTop: 24,
    marginBottom: 4,
  },
  content: {
    marginLeft: 8,
    flex: 1,
    justifyContent: 'space-between',
  },
  actionButton: {
    flex: 1,
  },
  answered: {
    marginTop: 12,
  },
});

export default SurveyCard;
