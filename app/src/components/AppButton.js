import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const AppButton = ({style, text, buttonStyle, onPress, ...rest}) => {
  return (
    <View style={[styles.container, style]}>
      <TouchableOpacity onPress={onPress}>
        <View style={[styles.button, buttonStyle]}>
          <Text
            style={[
              styles.text,
              buttonStyle ? {color: buttonStyle.color ?? '#8D30E6'} : undefined,
            ]}>
            {text}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
  },
  row: {
    flexDirection: 'row',
    flex: 1,
  },
  button: {
    flexDirection: 'row',
    borderRadius: 32,
    padding: 18,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  text: {
    color: '#8D30E6',
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
});

export default AppButton;
