import React from 'react';
import {StyleSheet, TextInput, View, Text} from 'react-native';

const AppTextInput = ({
  onChangeText,
  value,
  inputStyle,
  labelStyle,
  style,
  label,
  ...rest
}) => {
  return (
    <View style={[styles.row, style]}>
      <View style={styles.container}>
        <Text style={[styles.label, labelStyle]}>{label}</Text>
        <View style={styles.row}>
          <TextInput
            onChangeText={onChangeText}
            value={value}
            style={[styles.input, inputStyle]}
            {...rest}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1,
  },
  row: {
    flexDirection: 'row',
  },
  input: {
    backgroundColor: 'white',
    borderRadius: 32,
    paddingVertical: 16,
    paddingHorizontal: 24,
    flex: 1,
  },
  label: {
    marginVertical: 12,
    color: 'white',
    marginLeft: 8,
    fontSize: 16,
  },
});

export default AppTextInput;
