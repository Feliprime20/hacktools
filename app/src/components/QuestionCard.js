import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';

const QuestionCard = ({question, number, value, onChange}) => {
  return (
    <View style={styles.card}>
      <Text>
        {number}. {question.title}
      </Text>
      <TextInput
        placeholder="Resposta"
        style={styles.input}
        multiline={true}
        numberOfLines={5}
        value={value}
        onChangeText={(text) => onChange(text)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 16,
    borderRadius: 12,
    marginVertical: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    marginHorizontal: 12,
  },
  input: {
    marginTop: 24,
    marginBottom: 12,
    fontSize: 16,
    borderRadius: 4,
    borderColor: 'gray',
    borderWidth: 0.5,
    paddingHorizontal: 16,
    textAlignVertical: 'top',
  },
});

export default QuestionCard;
