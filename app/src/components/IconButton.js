import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

const IconButton = ({style, buttonStyle, onPress, name, size, color, text}) => {
  return (
    <TouchableOpacity onPress={onPress} style={style}>
      <View style={[styles.button, buttonStyle]}>
        <Icon name={name} size={size} color={color} />
        {text && <Text style={[styles.text, {color}]}>{text}</Text>}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    marginLeft: 8,
  },
});

export default IconButton;
