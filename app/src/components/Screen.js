import React from 'react';
import {StatusBar, StyleSheet} from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';

const Screen = ({hiddenStatusBar, children, background, style}) => {
  return (
    <SafeAreaView
      style={[
        styles.container,
        {backgroundColor: background ?? '#eee'},
        style,
      ]}>
      <StatusBar
        barStyle="light-content"
        backgroundColor="#620076"
        hidden={hiddenStatusBar ?? false}
      />
      {children}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Screen;
