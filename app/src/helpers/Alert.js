import {Alert as AlertDialog} from 'react-native';

export default class Alert {
  static show(title, message, onClick) {
    AlertDialog.alert(
      title,
      message,
      [
        {
          text: 'OK',
          onPress: () => {
            onClick?.call();
          },
        },
      ],
      {
        cancelable: false,
      },
    );
  }
}
