import api from './Api';
import {AlreadyRegistered, BadDataError} from './persistence/Errors';
import UserRepository from './persistence/UserRepository';

export default class UserService {
  async register({name, email, password}) {
    if (name.length === 0) {
      throw new BadDataError('Por favor informe seu nome.');
    }

    if (name.length < 3) {
      throw new BadDataError('O nome deve ter no mínimo 3 caracteres.');
    }

    if (name.length > 100) {
      throw new BadDataError('O nome deve ter no máximo 100 caracteres.');
    }

    if (email.length === 0) {
      throw new BadDataError('Por favor informe seu e-mail.');
    }

    if (email.length < 6) {
      throw new BadDataError('o e-mail deve ter no mínimo 6 caracteres.');
    }

    if (email.length > 254) {
      throw new BadDataError('o e-mail deve ter no máximo 254 caracteres.');
    }

    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (!re.test(email)) {
      throw new BadDataError('o e-mail inválido.');
    }

    if (password.length === 0) {
      throw new BadDataError('Por favor informe uma senha.');
    }

    if (password.length < 6) {
      throw new BadDataError('A senha deve ter no mínimo 6 caracteres.');
    }

    if (password.length > 20) {
      throw new BadDataError('A senha deve ter no máximo 20 caracteres.');
    }

    return new Promise((resolve, reject) => {
      api
        .post('/user', {name, email, password})
        .then((result) => {
          const {id, date, token} = result.data;
          const user = new UserRepository().insert({
            name,
            email,
            id,
            date,
            token,
          });
          resolve(user);
        })
        .catch((error) => {
          if (error.response?.status === 409) {
            reject(
              new AlreadyRegistered(
                'Já existe um usuário com o endereço de e-mail informado',
              ),
            );
          }

          reject(
            new Error('Não foi possível processar os dados. Tente novamente.'),
          );
        });
    });
  }

  async login(email, password) {
    if (email.length === 0) {
      throw new Error('Por favor informe seu e-mail.');
    }

    if (password.length === 0) {
      throw new Error('Por favor informe uma senha.');
    }

    return new Promise((resolve, reject) => {
      api
        .post('/user/login', {email, password})
        .then((result) => {
          const {id, date, name, token} = result.data;

          const user = new UserRepository().insert({
            name,
            email,
            id,
            date,
            token,
          });
          resolve(user);
        })
        .catch((error) => {
          if (error.response?.status === 401) {
            reject(new AlreadyRegistered('E-mail e ou senha incorretos.'));
          }

          reject(new Error('Não foi possível fazer o login. Tente novamente.'));
        });
    });
  }
}
