import axios from 'axios';
import config from '../config';
import UserRepository from './persistence/UserRepository';

const userRepository = new UserRepository();
const user = userRepository.get();

const api = axios.create({
  baseURL: config.baseUrl,
  headers: user ? {Authorization: `Bearer ${user.token}`} : undefined,
});
export default api;
