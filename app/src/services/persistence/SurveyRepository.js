import {AlreadyRegistered, BadDataError} from './Errors';
import Survey from './model/Survey';
import realm from './Realm';
import Question from './model/Question';

export default class SurveyRepository {
  create(userId, title) {
    const existent = realm
      .objects('Survey')
      .find((survey) => survey.title === title);

    if (title.length === 0) {
      throw new BadDataError('Informe o título do questionário');
    }

    if (existent) {
      throw new AlreadyRegistered('Já existe um questionário com esse título');
    }

    return new Promise((resolve, reject) => {
      try {
        realm.write(() => {
          const survey = realm.create('Survey', new Survey({title, userId}));
          resolve(survey);
        });
      } catch (error) {
        reject(
          new Error(
            'Não foi possível adicionar o questionário. Tente mais tarde.',
          ),
        );
      }
    });
  }

  async addQuestions(surveyId, questions) {
    return new Promise((resolve, reject) => {
      try {
        realm.write(() => {
          const survey = this.find(surveyId);

          const questionArray = questions.map(
            ({title}) => new Question({title, surveyId}),
          );

          survey.questions = questionArray;
          resolve(questionArray);
        });
      } catch (error) {
        throw new Error(
          'Não foi possível adicionar as questões. Tente novamente',
        );
      }
    });
  }

  find(id) {
    const result = realm.objects('Survey').filtered('id == $0 LIMIT(1)', id);
    if (result.length === 0) {
      return null;
    }

    return result[0];
  }

  findAll() {
    return realm.objects('Survey');
  }

  async delete(surveyId) {
    return new Promise((resolve, reject) => {
      try {
        realm.write(() => {
          const survey = this.find(surveyId);

          realm.delete(survey);
          resolve(true);
        });
      } catch (error) {
        resolve(false);
      }
    });
  }
}
