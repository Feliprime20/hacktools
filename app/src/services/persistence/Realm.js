import Realm from 'realm';
import Answer from './model/Answer';
import Question from './model/Question';
import Survey from './model/Survey';
import SurveyAnswer from './model/SurveyAnswer';
import User from './model/User';

const realm = new Realm({
  schema: [
    User.schema,
    Survey.schema,
    Question.schema,
    Answer.schema,
    SurveyAnswer.schema,
  ],
  schemaVersion: 7,
});

export default realm;
