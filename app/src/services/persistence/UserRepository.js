import realm from './Realm';
export default class UserRepository {
  async insert({name, email, id, date, token}) {
    return new Promise((resolve, reject) => {
      try {
        realm.write(() => {
          const user = realm.create('User', {name, email, id, date, token});
          resolve(user);
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  get() {
    let user = null;
    const users = realm.objects('User');

    if (users.length !== 0) {
      user = users[0];
    }

    return user;
  }
}
