import SurveyAnswer from './model/SurveyAnswer';
import realm from './Realm';
import Answer from './model/Answer';

export default class SurveyAnswerRepository {
  save(survey, answers, {latitude, longitude}) {
    return new Promise((resolve, reject) => {
      try {
        realm.write(() => {
          const existent = this.find(survey.id);

          if (!existent) {
            const surveyAnswer = new SurveyAnswer({
              surveyId: survey.id,
              latitude,
              longitude,
              answers: answers,
            });

            realm.create('SurveyAnswer', surveyAnswer);
            resolve(surveyAnswer);
            return;
          }

          const surveyAnswer = realm.objectForPrimaryKey(
            'SurveyAnswer',
            existent.id,
          );

          surveyAnswer.latitude = latitude;
          surveyAnswer.longitude = longitude;
          surveyAnswer.answers = answers;
          surveyAnswer.date = new Date();
          resolve(surveyAnswer);
        });
      } catch (error) {
        console.log(error);
        reject(
          new Error(
            'Não foi possível salvar as respostas do questionário. Tente novamente',
          ),
        );
      }
    });
  }

  find(surveyId) {
    const result = realm
      .objects('SurveyAnswer')
      .filtered('surveyId == $0 LIMIT(1)', surveyId);

    if (result.length === 0) {
      return null;
    }

    const surveyAnswer = new SurveyAnswer({
      id: result[0].id,
      date: result[0].date,
      latitude: result[0].latitude,
      longitude: result[0].longitude,
      surveyId: result[0].surveyId,
    });

    const answers = [];

    for (const answer of result[0].answers) {
      const question = realm.objectForPrimaryKey('Question', answer.questionId);

      answers.push(
        new Answer({
          id: answer.id,
          questionId: answer.questionId,
          surveyId: surveyAnswer.surveyId,
          value: answer.value,
          question: JSON.parse(JSON.stringify(question)),
        }),
      );
    }

    surveyAnswer.answers = answers;
    return surveyAnswer;
  }
}
