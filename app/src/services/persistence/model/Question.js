import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';

export default class Question {
  constructor({id, title, surveyId}) {
    this.title = title;
    this.surveyId = surveyId;
    this.id = id ?? uuidv4();
  }

  static schema = {
    name: 'Question',
    properties: {
      id: 'string',
      title: 'string',
      surveyId: 'string',
      survey: {type: 'Survey'},
    },
    primaryKey: 'id',
  };
}

export {Question};
