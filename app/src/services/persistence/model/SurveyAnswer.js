import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';

export default class SurveyAnswer {
  constructor({id, surveyId, date, latitude, longitude, answers}) {
    this.date = date ?? new Date();
    this.id = id ?? uuidv4();
    this.surveyId = surveyId;
    this.latitude = latitude;
    this.longitude = longitude;
    this.answers = answers ?? [];
  }

  static schema = {
    name: 'SurveyAnswer',
    properties: {
      id: 'string',
      surveyId: 'string',
      date: 'date',
      latitude: 'double',
      longitude: 'double',
      answers: {type: 'list', objectType: 'Answer'},
    },
    primaryKey: 'id',
  };
}

export {SurveyAnswer};
