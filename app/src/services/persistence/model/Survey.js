import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';

export default class Survey {
  constructor({id, title, userId, date}) {
    this.title = title;
    this.userId = userId;
    this.date = date ?? new Date();
    this.id = id ?? uuidv4();
    this.questions = [];
  }

  static schema = {
    name: 'Survey',
    properties: {
      id: 'string',
      userId: 'int',
      title: 'string',
      date: 'date',
      questions: {type: 'list', objectType: 'Question'},
    },
    primaryKey: 'id',
  };
}

export {Survey};
