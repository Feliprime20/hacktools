export default class User {
  constructor({id, name, email, date, token}) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.date = date;
    this.token = token;
  }

  static schema = {
    name: 'User',
    properties: {
      id: 'int',
      email: 'string',
      name: 'string',
      date: 'string',
      token: 'string',
    },
    primaryKey: 'id',
  };
}

export {User};
