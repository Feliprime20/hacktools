import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';

export default class Answer {
  constructor({questionId, surveyId, value, surveyAnswer, question, id}) {
    this.questionId = questionId;
    this.surveyId = surveyId;
    this.value = value;
    this.surveyAnswer = surveyAnswer;
    this.question = question;
    this.id = id ?? uuidv4();
  }

  static schema = {
    name: 'Answer',
    properties: {
      id: 'string',
      questionId: 'string',
      surveyId: 'string',
      value: 'string',
      surveyAnswer: {type: 'SurveyAnswer'},
    },
    primaryKey: 'id',
  };
}

export {Answer};
