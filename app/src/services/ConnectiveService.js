import NetInfo from '@react-native-community/netinfo';

export default class ConnectivityService {
  async isConnected() {
    const result = await NetInfo.fetch();
    return result.isConnected;
  }
}
