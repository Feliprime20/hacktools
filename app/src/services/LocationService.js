import GetLocation from 'react-native-get-location';

export class LocationPermissionDeniedError extends Error {}
export class LocationUnavaliableError extends Error {}
export class LocationError extends Error {}

export default class LocationService {
  async get() {
    try {
      const location = await GetLocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 10000,
      });
      return location;
    } catch (error) {
      const {code} = error;

      switch (code) {
        case 'UNAVAILABLE':
          throw new LocationUnavaliableError(
            'Não foi possível obter a localização pois os serviço está indisponível.',
          );

        case 'UNAUTHORIZED':
          throw new LocationPermissionDeniedError(
            'Não foi possível obter a localização. Tente novamente.',
          );

        default:
          throw new LocationError(
            'Não foi possível obter a localização. Tente novamente.',
          );
      }
    }
  }

  openSettings() {
    GetLocation.openAppSettings();
  }

  enableGps() {
    GetLocation.openGpsSettings();
  }
}
