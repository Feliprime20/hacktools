import realm from './persistence/Realm';

export default class SignOutService {
  execute() {
    realm.write(() => {
      realm.deleteAll();
    });
  }
}
