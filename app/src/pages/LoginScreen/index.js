import React, {useState} from 'react';
import {ActivityIndicator, Image, Text} from 'react-native';
import Logo from '../../assets/img/logo.png';
import AppButton from '../../components/AppButton';
import Alert from '../../helpers/Alert';
import AppTextInput from './../../components/AppTextInput';
import Screen from './../../components/Screen';
import ConnectivityService from './../../services/ConnectiveService';
import UserService from './../../services/UserService';
import styles from './styles';

const userService = new UserService();
const connectivityService = new ConnectivityService();

const LoginScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const handleLogin = async () => {
    if (!(await connectivityService.isConnected())) {
      Alert.show('Sem conexão', 'Verifique a conexão com a internet!');
      return;
    }

    try {
      setLoading(true);
      const user = await userService.login(email, password);
      navigation.navigate('Home', {user: JSON.stringify(user)});
    } catch (error) {
      Alert.show('Erro', error.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Screen hiddenStatusBar={true} background="#620076">
      <Image source={Logo} style={styles.logo} />
      <Text style={styles.title}>Login</Text>
      <AppTextInput
        onChangeText={(text) => setEmail(text)}
        value={email}
        placeholder="*E-mail"
        label="*E-mail"
        autoCompleteType="email"
        keyboardType="email-address"
      />
      <AppTextInput
        onChangeText={(text) => setPassword(text)}
        value={password}
        placeholder="*Senha"
        label="*Senha"
        secureTextEntry={true}
      />
      <AppButton text="Entrar" style={styles.button} onPress={handleLogin} />
      {loading && (
        <ActivityIndicator
          size="large"
          color="#ffffff"
          style={styles.loading}
        />
      )}
    </Screen>
  );
};

export default LoginScreen;
