import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  logo: {
    width: 100,
    height: 100,
    borderRadius: 16,
  },
  title: {
    marginTop: 24,
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
  button: {
    marginTop: 56,
  },
  loading: {
    marginTop: 16,
  },
});

export default styles;
