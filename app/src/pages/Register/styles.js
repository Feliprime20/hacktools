import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  scroll: {flex: 1, alignSelf: 'stretch', backgroundColor: '#620076'},
  container: {
    justifyContent: 'flex-start',
  },
  logo: {
    width: 100,
    height: 100,
    borderRadius: 16,
    marginTop: 24,
  },
  title: {
    marginTop: 24,
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
  firstInput: {
    marginTop: 8,
  },
  register: {marginTop: 56},
  back: {marginTop: 12},
  loading: {marginTop: 12},
});

export default styles;
