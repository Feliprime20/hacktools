import React, {useCallback, useState} from 'react';
import {ActivityIndicator, Image, ScrollView, Text} from 'react-native';
import Logo from '../../assets/img/logo.png';
import Alert from '../../helpers/Alert';
import ConnectivityService from '../../services/ConnectiveService';
import AppButton from './../../components/AppButton';
import AppTextInput from './../../components/AppTextInput';
import Screen from './../../components/Screen';
import UserService from './../../services/UserService';
import styles from './styles';

const userService = new UserService();
const connectivityService = new ConnectivityService();

const RegisterScreen = ({navigation}) => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const showError = useCallback((message) => {
    Alert.show('Verifique os campos', message);
  }, []);

  const handleRegister = useCallback(async () => {
    if (!(await connectivityService.isConnected())) {
      Alert.show('Sem conexão', 'Verifique a conexão com a internet!');
      return;
    }

    try {
      setLoading(true);
      const user = await userService.register({name, email, password});
      navigation.reset({
        routes: [{name: 'Home', params: {user: JSON.stringify(user)}}],
      });
    } catch (error) {
      showError(error.message);
    } finally {
      setLoading(false);
    }
  }, [name, email, password, showError, navigation]);

  return (
    <ScrollView style={styles.scroll}>
      <Screen background="#620076" style={styles.container}>
        <Image source={Logo} style={styles.logo} />
        <Text style={styles.title}>Cadastrar</Text>
        <AppTextInput
          onChangeText={(text) => setName(text)}
          value={name}
          placeholder="*Nome"
          label="*Nome"
          style={styles.firstInput}
        />
        <AppTextInput
          onChangeText={(text) => setEmail(text)}
          value={email}
          placeholder="*E-mail"
          label="*E-mail"
          autoCompleteType="email"
          keyboardType="email-address"
        />
        <AppTextInput
          onChangeText={(text) => setPassword(text)}
          value={password}
          placeholder="*Senha"
          label="*Senha"
          secureTextEntry={true}
        />
        <AppButton
          text="Cadastrar"
          style={styles.register}
          onPress={!loading ? handleRegister : undefined}
          enable={!loading}
        />
        {loading && (
          <ActivityIndicator
            size="large"
            color="#ffffff"
            style={styles.loading}
          />
        )}
      </Screen>
    </ScrollView>
  );
};

export default RegisterScreen;
