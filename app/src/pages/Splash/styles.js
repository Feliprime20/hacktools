import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  logo: {
    width: 200,
    height: 200,
    borderRadius: 16,
  },
  appName: {
    marginTop: 48,
    fontSize: 36,
    fontWeight: 'bold',
    color: 'white',
  },
  buttons: {
    alignSelf: 'stretch',
  },
  firstButton: {
    marginTop: 16,
    marginBottom: 48,
  },
  register: {
    backgroundColor: '#A68AFF',
    color: 'white',
  },
});

export default styles;
