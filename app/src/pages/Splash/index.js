import React, {useEffect} from 'react';
import {Image, Text, View} from 'react-native';
import Logo from '../../assets/img/logo.png';
import AppButton from '../../components/AppButton';
import Screen from './../../components/Screen';
import UserRepository from './../../services/persistence/UserRepository';
import styles from './styles';

const userRepository = new UserRepository();

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    const user = userRepository.get();
    if (user) {
      navigation.reset({
        routes: [{name: 'Home', params: {user: JSON.stringify(user)}}],
      });
      return;
    }
  }, [navigation]);

  const handleRegister = () => {
    navigation.navigate('Register');
  };

  const handleLogin = () => {
    navigation.navigate('Login');
  };

  return (
    <Screen hiddenStatusBar={true} background="#620076">
      <Image source={Logo} style={styles.logo} />
      <Text style={styles.appName}>HackTools</Text>

      <View style={styles.buttons}>
        <AppButton
          text="Já tenho uma conta"
          style={styles.firstButton}
          onPress={handleLogin}
        />
        <AppButton
          text="Cadastrar"
          buttonStyle={styles.register}
          onPress={handleRegister}
        />
      </View>
    </Screen>
  );
};

export default SplashScreen;
