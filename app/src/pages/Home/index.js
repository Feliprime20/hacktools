import {StackActions, useIsFocused} from '@react-navigation/native';
import React, {useCallback, useEffect, useState} from 'react';
import {FlatList, Image, Text, View, Alert} from 'react-native';
import Logo from '../../assets/img/logo.png';
import FAB from '../../components/FAB';
import IconButton from './../../components/IconButton';
import Screen from './../../components/Screen';
import SurveyCard from './../../components/SurveyCard';
import SignOutService from './../../services/SignOutService';
import styles from './styles';
import SurveyRepository from '../../services/persistence/SurveyRepository';
import SurveyAnswerRepository from '../../services/persistence/SurveyAnswerRepository';

const signOutService = new SignOutService();
const surveyRepository = new SurveyRepository();
const surveyAnswerRepository = new SurveyAnswerRepository();

const HomeScreen = ({route, navigation}) => {
  const user = JSON.parse(route.params.user);
  const [surveys, setSurveys] = useState([]);
  const isFocused = useIsFocused();

  const handleExit = useCallback(() => {
    signOutService.execute();
    navigation.dispatch(StackActions.replace('Splash'));
  }, [navigation]);

  const handleAddSurvey = useCallback(() => {
    navigation.navigate('AddSurveyModal');
  }, [navigation]);

  useEffect(() => {
    if (!isFocused) {
      return;
    }

    const results = surveyRepository.findAll();
    setSurveys(results);
  }, [isFocused]);

  const showError = useCallback((message) => {
    Alert.show('Erro', message);
  }, []);

  const handleDelete = useCallback(
    async (survey) => {
      const result = await surveyRepository.delete(survey.id);
      if (!result) {
        showError('Não foi possível apagar o questionário. Tente novamente');
      }
      setSurveys(surveyRepository.findAll());
    },
    [showError],
  );

  const handleConfigure = useCallback(
    (survey) => {
      navigation.navigate('AddQuestions', {survey: JSON.stringify(survey)});
    },
    [navigation],
  );

  const handleAnswer = useCallback(
    (survey) => {
      navigation.navigate('AnswerSurvey', {survey: JSON.stringify(survey)});
    },
    [navigation],
  );

  return (
    <Screen>
      <View style={styles.container}>
        <View style={[styles.row, styles.appHeader]}>
          <View style={styles.row}>
            <Image source={Logo} style={styles.logo} />
            <Text style={styles.appName}>HackTools</Text>
          </View>
          <IconButton
            name="log-out"
            size={28}
            color="#9e9e9e"
            onPress={handleExit}
          />
        </View>
        <Text style={styles.greeting}>Bem Vindo, {user.name}!</Text>

        <Text style={styles.header}>Questionários</Text>

        {surveys.length > 0 && (
          <FlatList
            data={surveys}
            keyExtractor={(item) => item.id}
            renderItem={({item}) => (
              <SurveyCard
                survey={item}
                deleteAction={handleDelete}
                configureAction={handleConfigure}
                answerAction={handleAnswer}
                hasAnswer={surveyAnswerRepository.find(item.id)}
                editAnswersAction={handleAnswer}
              />
            )}
            style={styles.list}
          />
        )}
        {surveys.length === 0 && (
          <Text style={styles.emptyState}>Sem Questionarios</Text>
        )}
        <FAB
          name="plus"
          size={24}
          color="#FFF"
          style={styles.fab}
          onPress={handleAddSurvey}
        />
      </View>
    </Screen>
  );
};

export default HomeScreen;
