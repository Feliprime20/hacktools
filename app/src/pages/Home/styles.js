import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
  },
  logo: {
    width: 50,
    height: 50,
    borderRadius: 8,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  appName: {
    marginLeft: 12,
    fontSize: 16,
  },
  greeting: {
    marginTop: 18,
    fontSize: 16,
  },
  appHeader: {
    justifyContent: 'space-between',
  },
  header: {
    marginTop: 24,
    fontSize: 20,
    fontWeight: 'bold',
  },
  fab: {
    position: 'absolute',
    bottom: 16,
    right: 0,
  },
  list: {
    marginTop: 12,
  },
  emptyState: {
    flex: 1,
    fontSize: 20,
    alignSelf: 'stretch',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
});

export default styles;
