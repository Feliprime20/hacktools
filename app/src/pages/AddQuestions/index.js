import React, {useCallback, useState} from 'react';
import {Alert, FlatList, Text, View} from 'react-native';
import IconButton from '../../components/IconButton';
import Question from '../../services/persistence/model/Question';
import SurveyRepository from '../../services/persistence/SurveyRepository';
import FAB from './../../components/FAB';
import Screen from './../../components/Screen';
import styles from './styles';

const surveyRepository = new SurveyRepository();

const AddQuestionsScreen = ({route, navigation}) => {
  const survey = JSON.parse(route.params.survey);
  const [questions, setQuestions] = useState(survey.questions);

  const handleAddQuestion = useCallback(() => {
    navigation.navigate('AddQuestionModal', {
      onQuestionAdded: ({title}) => {
        if (questions.find((item) => item.title === title)) {
          throw new Error('Já existem uma questão com esse título');
        }

        setQuestions((oldQuestions) => [
          ...oldQuestions,
          new Question({title, surveyId: survey.id}),
        ]);
      },
    });
  }, [navigation, questions, survey]);

  const removeQuestion = useCallback(
    (question) => {
      const items = questions.filter((item) => item.title !== question.title);
      setQuestions(items);
    },
    [questions],
  );

  const showError = useCallback((message) => {
    Alert.show('Erro', message);
  }, []);

  const handleSave = useCallback(async () => {
    try {
      if (questions.length === 0) {
        showError('Adicione pelo menos uma questão.');
        return;
      }

      await surveyRepository.addQuestions(survey.id, questions);
      navigation.goBack();
    } catch (error) {
      showError(error.message);
    }
  }, [questions, survey, navigation, showError]);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <IconButton name="check" size={24} color="#FFF" onPress={handleSave} />
      ),
    });
  }, [navigation, handleSave]);

  return (
    <Screen>
      <View
        style={[
          styles.container,
          questions.length === 0 ? {justifyContent: 'center'} : undefined,
        ]}>
        {questions.length === 0 && (
          <Text style={styles.emptyState}>Sem questões</Text>
        )}
        {questions.length > 0 && (
          <FlatList
            keyExtractor={(item) => item.title}
            data={questions}
            renderItem={({item, index}) => (
              <View style={styles.questionCard}>
                <Text>
                  {index + 1}. {item.title}
                </Text>
                <IconButton
                  name="trash"
                  size={24}
                  color="#f44336"
                  onPress={() => removeQuestion(item)}
                />
              </View>
            )}
          />
        )}
        <FAB
          name="plus"
          size={24}
          color="#FFF"
          style={styles.fab}
          onPress={handleAddQuestion}
        />
      </View>
    </Screen>
  );
};

export default AddQuestionsScreen;
