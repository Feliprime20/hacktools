import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  fab: {
    position: 'absolute',
    bottom: 32,
    right: 16,
  },
  greenFab: {
    backgroundColor: '#4caf50',
  },
});

export default styles;
