import React, {useCallback, useState} from 'react';
import {Alert as AlertDialog, ScrollView, View} from 'react-native';
import Loading from '../../components/Loading';
import QuestionCard from '../../components/QuestionCard';
import Answer from '../../services/persistence/model/Answer';
import FAB from './../../components/FAB';
import Alert from './../../helpers/Alert';
import LocationService, {
  LocationPermissionDeniedError,
  LocationUnavaliableError,
} from './../../services/LocationService';
import styles from './styles';
import SurveyAnswerRepository from '../../services/persistence/SurveyAnswerRepository';

const locationService = new LocationService();
const surveyAnswerRepository = new SurveyAnswerRepository();

const AnswerSurveyScreen = ({navigation, route}) => {
  const survey = JSON.parse(route.params.survey);

  const [answers, setAnswers] = useState(
    surveyAnswerRepository.find(survey.id)?.answers ??
      survey.questions.map(
        (item) =>
          new Answer({
            questionId: item.id,
            surveyId: survey.id,
            value: '',
            question: item,
          }),
      ),
  );

  const [loading, setLoading] = useState(false);

  const handleChange = (text, index) => {
    const newAnswers = [...answers];
    newAnswers[index].value = text;
    setAnswers(newAnswers);
  };

  const handleSave = useCallback(async () => {
    const emptys = answers.filter((item) => item.value === '');

    if (emptys.length > 0) {
      Alert.show('Erro', 'Verifique se todas as questões foram respondidas.');
      return;
    }

    let location;
    setLoading(true);

    try {
      location = await locationService.get();
    } catch (error) {
      if (error instanceof LocationPermissionDeniedError) {
        AlertDialog.alert(
          'Permissão necessária',
          'Para salvar as respostas do questionário é necessário acessar a localização atual.',
          [
            {
              text: 'Cancelar',
            },
            {
              text: 'Ir para as configurações',
              onPress: () => {
                locationService.openSettings();
              },
            },
          ],
          {
            cancelable: false,
          },
        );

        return;
      }

      if (error instanceof LocationUnavaliableError) {
        AlertDialog.alert(
          'Localização não disponível',
          'Parece que a localização do dispositivo está desativada. Deseja ativa-la?',
          [
            {
              text: 'Cancelar',
            },
            {
              text: 'Ativar',
              onPress: () => {
                locationService.enableGps();
              },
            },
          ],
          {
            cancelable: false,
          },
        );

        return;
      }

      Alert.show('Erro de localização', 'error.message');
    } finally {
      setLoading(false);
    }

    if (!location) {
      Alert.show(
        'Erro',
        'Não foi possível obter a localização. Tente novamente',
      );

      return;
    }

    try {
      await surveyAnswerRepository.save(survey, answers, location);
      navigation.goBack();
    } catch (error) {
      Alert.show('Erro', error.message);
    }
  }, [survey, answers, navigation]);

  return (
    <View style={styles.container}>
      <ScrollView style={styles.scroll}>
        <View>
          {answers.map((item, index) => (
            <QuestionCard
              key={item.questionId}
              question={item.question}
              number={index + 1}
              value={item.value}
              onChange={(text) => {
                handleChange(text, index);
              }}
            />
          ))}
        </View>
      </ScrollView>
      <FAB
        name="check"
        size={24}
        color="#FFF"
        style={styles.fab}
        buttonStyle={styles.greenFab}
        onPress={handleSave}
      />
      {loading && <Loading />}
    </View>
  );
};

export default AnswerSurveyScreen;
