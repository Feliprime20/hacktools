// In App.js in a new project

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import * as React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import AddQuestion from './modals/AddQuestion/index';
import AddSurvey from './modals/AddSurvey';
import AddQuestionsScreen from './pages/AddQuestions/index';
import AnswerSurveyScreen from './pages/AnswerSurvey/index';
import HomeScreen from './pages/Home';
import RegisterScreen from './pages/Register/index';
import SplashScreen from './pages/Splash/index';
import LoginScreen from './pages/LoginScreen/index';

const MainStack = createStackNavigator();
const RootStack = createStackNavigator();

function MainStackNav() {
  return (
    <MainStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#620076',
        },
        headerTintColor: '#FFFFFF',
      }}>
      <MainStack.Screen
        name="Splash"
        component={SplashScreen}
        options={{headerShown: false, headerTintColor: '#620076'}}
      />
      <MainStack.Screen
        name="Register"
        component={RegisterScreen}
        options={{headerShown: false, headerTintColor: '#620076'}}
      />
      <MainStack.Screen
        name="Home"
        component={HomeScreen}
        options={{headerShown: false, headerTintColor: '#620076'}}
      />
      <MainStack.Screen
        name="AddQuestions"
        component={AddQuestionsScreen}
        options={({route}) => ({
          headerShown: true,
          headerTitle: `Questões de ${JSON.parse(route.params.survey).title}`,
        })}
      />
      <MainStack.Screen
        name="AnswerSurvey"
        component={AnswerSurveyScreen}
        options={({route}) => ({
          headerShown: true,
          headerTitle: `${JSON.parse(route.params.survey).title}`,
        })}
      />
      <MainStack.Screen
        name="Login"
        component={LoginScreen}
        options={{headerShown: false, headerTintColor: '#620076'}}
      />
    </MainStack.Navigator>
  );
}

function App() {
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <RootStack.Navigator
          mode="modal"
          screenOptions={{
            headerShown: false,
            cardStyle: {backgroundColor: 'transparent'},
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({current: {progress}}) => ({
              cardStyle: {
                opacity: progress.interpolate({
                  inputRange: [0, 0.5, 0.9, 1],
                  outputRange: [0, 0.25, 0.7, 1],
                }),
              },
              overlayStyle: {
                opacity: progress.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, 0.5],
                  extrapolate: 'clamp',
                }),
              },
            }),
          }}>
          <RootStack.Screen
            name="Main"
            component={MainStackNav}
            options={{headerShown: false}}
          />
          <RootStack.Screen
            name="AddSurveyModal"
            component={AddSurvey}
            options={{headerShown: false}}
          />
          <RootStack.Screen
            name="AddQuestionModal"
            component={AddQuestion}
            options={{headerShown: false}}
          />
        </RootStack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}

export default App;
