import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  card: {
    backgroundColor: 'white',
    borderRadius: 16,
    marginHorizontal: 36,
    padding: 16,
  },
  title: {
    fontSize: 16,
    fontWeight: '700',
  },
  input: {
    marginTop: 24,
    fontSize: 16,
    borderRadius: 4,
    borderColor: 'gray',
    borderWidth: 0.5,
    paddingHorizontal: 16,
  },
  row: {
    flexDirection: 'row',
    marginTop: 32,
    marginBottom: 16,
    justifyContent: 'flex-end',
  },
  button: {
    marginHorizontal: 16,
  },
  btnText: {
    color: '#8D30E6',
  },
  error: {
    fontSize: 14,
    color: '#f44336',
    marginVertical: 4,
  },
});

export default styles;
