import React, {useCallback, useState} from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import styles from './styles';

const AddQuestion = ({route, navigation}) => {
  const [title, setTtitle] = useState('');
  const [message, setMessage] = useState('');
  const {onQuestionAdded} = route.params;

  const handleClose = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const handleAdd = useCallback(async () => {
    try {
      onQuestionAdded({title: title.trim()});
      navigation.goBack();
    } catch (error) {
      setMessage(error.message);
    }
  }, [navigation, title, onQuestionAdded]);

  return (
    <View style={styles.container}>
      <View style={styles.card}>
        <Text style={styles.title}>Adicionar Questão</Text>
        <TextInput
          onChangeText={(text) => setTtitle(text)}
          value={title}
          placeholder="*Título"
          style={styles.input}
        />
        {message.length > 0 && <Text style={styles.error}>{message}</Text>}

        <View style={styles.row}>
          <TouchableOpacity style={styles.button} onPress={handleClose}>
            <Text style={styles.btnText}>Cancelar</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={handleAdd}>
            <Text style={styles.btnText}>Ok</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default AddQuestion;
