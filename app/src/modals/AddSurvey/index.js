import React, {useCallback, useState} from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import SurveyRepository from '../../services/persistence/SurveyRepository';
import UserRepository from './../../services/persistence/UserRepository';
import styles from './styles';

const surveyRepository = new SurveyRepository();
const userRepository = new UserRepository();

const AddSurvey = ({navigation}) => {
  const [title, setTtitle] = useState('');
  const [message, setMessage] = useState('');

  const handleClose = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const handleAdd = useCallback(async () => {
    try {
      const survey = await surveyRepository.create(
        userRepository.get().id,
        title,
      );
      navigation.goBack();
      navigation.navigate('AddQuestions', {survey: JSON.stringify(survey)});
    } catch (error) {
      setMessage(error.message);
    }
  }, [navigation, title]);

  return (
    <View style={styles.container}>
      <View style={styles.card}>
        <Text style={styles.title}>Adicionar Questionário</Text>
        <TextInput
          onChangeText={(text) => setTtitle(text)}
          value={title}
          placeholder="*Título"
          style={styles.input}
        />
        {message.length > 0 && <Text style={styles.error}>{message}</Text>}

        <View style={styles.row}>
          <TouchableOpacity style={styles.button} onPress={handleClose}>
            <Text style={styles.btnText}>Cancelar</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={handleAdd}>
            <Text style={styles.btnText}>Ok</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default AddSurvey;
